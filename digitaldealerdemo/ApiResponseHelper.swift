//
//  ApiResponseHelper.swift
//  digitaldealerdemo
//
//  Created by Lapin, Andy (KBB) on 10/6/15.
//  Copyright © 2015 Kelley Blue Book. All rights reserved.
//

import UIKit

class ApiResponseHelper: NSObject {

    static func yearFromApiAi(dict: NSDictionary) -> String {
        
        return self.valueFromApiAiParameters("year", withDictionary: dict)
    }
    
    static func makeFromApiAi(dict: NSDictionary) -> String {
        
        return self.valueFromApiAiParameters("make", withDictionary: dict)
    }
    
    static func modelFromApiAi(dict: NSDictionary) -> String {
        
        return self.valueFromApiAiParameters("model", withDictionary: dict)
    }
    
    static func valueFromApiAiParameters(parameter: String, withDictionary dict: NSDictionary) -> String {
        var param: String = ""
        if let result = dict["result"] as? NSDictionary {
            if let parameters = result["parameters"] as? NSDictionary {
                param = parameters[parameter] as? String ?? ""
            }
        }
        return param
    }
    
    static func valueFromApiAiMetadata(parameter: String, withDictionary dict: NSDictionary) -> String {
        var param: String = ""
        if let result = dict["result"] as? NSDictionary {
            if let parameters = result["metadata"] as? NSDictionary {
                param = parameters[parameter] as? String ?? ""
            }
        }
        return param
    }
    
}
