//
//  Beacon.swift
//  digitaldealerdemo
//
//  Created by Lapin, Andy (KBB) on 10/5/15.
//  Copyright © 2015 Kelley Blue Book. All rights reserved.
//

import UIKit

class Beacon : NSObject {
    var name: String?
    var x: Float = 0
    var y: Float = 0
    var major: NSNumber?
    
    init(name: String, x : CGFloat, y : CGFloat, major: NSNumber) {
        self.name = name
        self.x = Float(x)
        self.y = Float(y)
        self.major = major
    }
    
    init(dict: NSDictionary) {
        self.name = dict["name"] as? String
        self.x = dict["x"] as! Float
        self.y = dict["y"] as! Float
    }
    
    func toDictionary() -> NSDictionary {
        let dict = NSDictionary()
        dict.setValue(self.name, forKey: "name")
        dict.setValue(self.x, forKey: "x")
        dict.setValue(self.y, forKey: "y")
        return dict
    }
}
