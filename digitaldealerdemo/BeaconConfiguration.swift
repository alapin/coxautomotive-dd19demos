//
//  BeaconConfiguration.swift
//  digitaldealerdemo
//
//  Created by Lapin, Andy (KBB) on 10/5/15.
//  Copyright © 2015 Kelley Blue Book. All rights reserved.
//

import UIKit

class BeaconConfiguration : NSObject {
    
    var upDirection: Float = 0.0
    var beacons: [Beacon]? = Array<Beacon>()
    
    init(dict: NSDictionary) {
        self.upDirection = (dict["upDirection"] as! NSNumber).floatValue
        let array: NSArray = dict["beacons"] as! NSArray
        
        self.beacons = Array<Beacon>();
        for item in array {
            self.beacons?.append(Beacon(dict: item as! NSDictionary))
        }
    }
    
    override init() {
        
    }
}
