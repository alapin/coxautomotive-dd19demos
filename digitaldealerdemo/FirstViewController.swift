//
//  FirstViewController.swift
//  digitaldealerdemo
//
//  Created by Lapin, Andy (KBB) on 9/25/15.
//  Copyright © 2015 Kelley Blue Book. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation
import ApiAI

// These are from EstimoteSDK/ESTBeaconDefinitions but weren't identified by Swift
let ESTIMOTE_PROXIMITY_UUID : NSUUID = NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!
let ESTIMOTE_MACBEACON_PROXIMITY_UUID : NSUUID = NSUUID(UUIDString: "08D4A950-80F0-4D42-A14B-D53E063516E6")!
let ESTIMOTE_IOSBEACON_PROXIMITY_UUID : NSUUID = NSUUID(UUIDString: "8492E75F-4FD6-469D-B132-043FE94921D8")!

class FirstViewController: UIViewController, ESTBeaconManagerDelegate, CLLocationManagerDelegate {
    
    let beaconManager: ESTBeaconManager = ESTBeaconManager()
    let locationManager: CLLocationManager = CLLocationManager()
    let synth: AVSpeechSynthesizer = AVSpeechSynthesizer()
    var currentTrueHeading: Double = 0.0
    
    var frontLeftBeacon: Beacon?
    var backLeftBeacon: Beacon?
    var frontRightBeacon: Beacon?
    var backRightBeacon: Beacon?
    var targetBeacon: Beacon?
    var playedArrivalAlert: Bool = false
    
    @IBOutlet var activity: UIActivityIndicatorView? = nil
    @IBOutlet var startListeningButton: UIButton? = nil
    @IBOutlet var stopListeningButton: UIButton? = nil
    @IBOutlet var useVAD: UISwitch? = nil
    @IBOutlet var voiceRequestButton: AIVoiceRequestButton? = nil;
    @IBOutlet var lastSearchLabel: UILabel!
    
    var currentVoiceRequest: AIVoiceRequest? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Test text-to-speech
//        let utterance: AVSpeechUtterance = AVSpeechUtterance(string: "Hello World")
//        let synth: AVSpeechSynthesizer = AVSpeechSynthesizer()
//        synth.speakUtterance(utterance)
        
        // Setup ranging beacons
        let region: ESTBeaconRegion = ESTBeaconRegion(proximityUUID: ESTIMOTE_PROXIMITY_UUID, identifier: "EstimoteSampleRegion");
        self.beaconManager.delegate = self;
        self.beaconManager.returnAllRangedBeaconsAtOnce = true;
        
        if (ESTBeaconManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined) {
            self.beaconManager.requestAlwaysAuthorization();
        }
        else if (ESTBeaconManager.authorizationStatus() == CLAuthorizationStatus.Denied) {
            let alertController = UIAlertController(title: "Location Error", message:
                "Please allow access to location services", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil));
            
            self.presentViewController(alertController, animated: true, completion: nil);
        }
        else {
            self.beaconManager.startRangingBeaconsInRegion(region);
            // Start tracking headings
            self.locationManager.startUpdatingHeading()
            self.locationManager.delegate = self
        }
        
        
        self.changeStateToStop();
        self.voiceRequestButton?.color = UIColor.yellowColor();
        self.voiceRequestButton?.iconColor = UIColor .blueColor();
        
        self.voiceRequestButton?.successCallback = {(AnyObject response) -> Void in
            NSLog(response.description)
            let responseDict: NSDictionary = response as! NSDictionary
            let year: String = ApiResponseHelper.yearFromApiAi(responseDict)
            let make: String = ApiResponseHelper.makeFromApiAi(responseDict)
            let modelName: String = ApiResponseHelper.modelFromApiAi(responseDict)
            let intentName: String = ApiResponseHelper.valueFromApiAiMetadata("intentName", withDictionary: responseDict)
            
            if (intentName == "FindOnLot") {
                self.lastSearchLabel.text = String(format: "You searched for %@ %@ %@ and we have some in stock", year, make, modelName)
                
                let voice : String = self.directionToBeaconForValue(modelName)
                print(voice)
                let utterance: AVSpeechUtterance = AVSpeechUtterance(string: voice)
                self.synth.speakUtterance(utterance)
                self.configuretargetBeaconForValue(modelName)
            }
            else if (intentName == "Buy Now") {
                self.lastSearchLabel.text = "Please proceed to sales."
                let voice : String = self.directionToBeaconForValue("Sales")
                print(voice)
                let utterance: AVSpeechUtterance = AVSpeechUtterance(string: voice)
                let synth: AVSpeechSynthesizer = AVSpeechSynthesizer()
                synth.speakUtterance(utterance)
                self.configuretargetBeaconForValue("Sales")
            }
            else {
                self.lastSearchLabel.text = "Sorry, I didn't understand what you were asking. Please try again"
            }
        }
        
        self.voiceRequestButton?.failureCallback = {(NSError error) -> Void in
            NSLog(error.description);
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Set up locations for each beacon
        let config: BeaconConfiguration = (UIApplication.sharedApplication().delegate as! AppDelegate).beaconConfiguraiton!
        
        if (config.beacons?.count > 0) {
            for item in config.beacons!
            {
                let beacon: Beacon = item as Beacon
                if (beacon.x < Float(self.view.frame.width / 2.0 - 50.0) && beacon.y < Float(self.view.frame.height / 2.0 - 27.5)) {
                    // Top Left
                    self.frontLeftBeacon = item
                }
                else if (beacon.x < Float(self.view.frame.width / 2.0 - 50.0) && beacon.y > Float(self.view.frame.height / 2.0 - 27.5)) {
                    // Bottom Left
                    self.backLeftBeacon = item
                }
                else if (beacon.x > Float(self.view.frame.width / 2.0 - 50.0) && beacon.y < Float(self.view.frame.height / 2.0 - 27.5)) {
                    // Top Right
                    self.frontRightBeacon = item
                }
                else {
                    // Bottom Right
                    self.backRightBeacon = item
                }
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated);
        
        self.currentVoiceRequest?.cancel();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        return UIInterfaceOrientationMask.Portrait;
    }
    
    override func shouldAutorotate() -> Bool {
        return false;
    }
    
    func beaconManager(manager: ESTBeaconManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: ESTBeaconRegion!) {
        
        if (beacons.count > 0) {
//            NSLog("Number of beacons found: %i", beacons.count);
            if (self.targetBeacon != nil && self.playedArrivalAlert == false) {
                for item in beacons {
                    if (item.major == self.targetBeacon!.major) {
                        if (item.distance!.floatValue < 2.0) {

                            let utterance: AVSpeechUtterance = AVSpeechUtterance(string: String(format: "You reached the %@", self.targetBeacon!.name!))
                            self.synth.speakUtterance(utterance)
                            self.playedArrivalAlert = true;
                        }
                    }
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.currentTrueHeading = newHeading.trueHeading;
//        print("Current heading: %.1f", self.currentTrueHeading)
    }
    
    @IBAction func startListening(sender: UIButton) {
        self.changeStateToListening();
        
        let apiai = ApiAI.sharedApiAI();
        
        let request = apiai.voiceRequest();
        
        if let vad = self.useVAD {
            request.useVADForAutoCommit = vad.on;
        };
        
        request.setCompletionBlockSuccess({[unowned self] (AIRequest request, AnyObject response) -> Void in
            
            self.changeStateToStop();
            NSLog(response.description);
            
            }, failure: { (AIRequest request, NSError error) -> Void in
                self.changeStateToStop();
                NSLog(error.description);
        });
        
        self.currentVoiceRequest = request
        
        apiai.enqueue(request);
    }
    
    @IBAction func stopListening(sender: UIButton) {
        self.currentVoiceRequest?.commitVoice();
    }
    
    func changeStateToStop() {
        self.activity?.stopAnimating();
        self.startListeningButton?.enabled = true;
        self.stopListeningButton?.enabled = false;
        
        self.useVAD?.enabled = true;
    }
    
    func changeStateToListening() {
        self.activity?.startAnimating();
        self.startListeningButton?.enabled = false;
        self.stopListeningButton?.enabled = true;
        
        self.useVAD?.enabled = false;
    }

    
    func valueForBeaconWithMajor(major: NSNumber) -> String {
        
        if (major.integerValue == 47301) {
            // Blue A: Jetta
            return "Jetta"
        }
        else if (major.integerValue == 10331) {
            // Green A: Camry
            return "Camry"
        }
        else if (major.integerValue == 61998) {
            // Green B: Volt
            return "Volt"
        }
        else if (major.integerValue == 1886) {
            // Purple A: Sales
            return "Sales"
        }
        return "Unknown"
    }
    
    func directionToBeaconForValue(beaconValue: String) -> String {
        let roomOffset: Double = -10
        
        if (self.frontLeftBeacon!.name == beaconValue) {
            if (self.currentTrueHeading + roomOffset > 300 && self.currentTrueHeading + roomOffset < 330) {
                // Straight
                return "Go straight."
            }
            else if (self.currentTrueHeading + roomOffset > 330 || self.currentTrueHeading + roomOffset < 45) {
                // turn left
                return "Turn to your left and walk straight"
            }
            else if (self.currentTrueHeading + roomOffset > 45 && self.currentTrueHeading + roomOffset < 225) {
                // turn around
                return "Turn around"
            }
            else {
                // turn right
                return "Turn to your right and walk straight"
            }
        }
        else if (self.backLeftBeacon!.name == beaconValue) {
            if (self.currentTrueHeading + roomOffset > 210 && self.currentTrueHeading + roomOffset < 240) {
                return "Go straight."
            }
            else if (self.currentTrueHeading + roomOffset > 240 && self.currentTrueHeading + roomOffset < 315) {
                return "Turn to your left and walk straight"
            }
            else if (self.currentTrueHeading + roomOffset > 315 || self.currentTrueHeading + roomOffset < 135) {
                return "Turn around"
            }
            else {
                return "Turn to your right and walk straight"
            }
        }
        else if (self.frontRightBeacon!.name == beaconValue) {
            if (self.currentTrueHeading + roomOffset > 30 && self.currentTrueHeading + roomOffset < 60) {
                return "Go straight."
            }
            else if (self.currentTrueHeading + roomOffset > 60 && self.currentTrueHeading + roomOffset < 135) {
                return "Turn to your left and walk straight"
            }
            else if (self.currentTrueHeading + roomOffset > 135 && self.currentTrueHeading + roomOffset < 315) {
                return "Turn around"
            }
            else {
                return "Turn to your right and walk straight"
            }
        }
        else if (self.backRightBeacon!.name == beaconValue) {
            if (self.currentTrueHeading + roomOffset > 120 && self.currentTrueHeading + roomOffset < 150) {
                return "Go straight."
            }
            else if (self.currentTrueHeading + roomOffset > 150 && self.currentTrueHeading + roomOffset < 225) {
                return "Turn to your left and walk straight"
            }
            else if (self.currentTrueHeading + roomOffset > 225 || self.currentTrueHeading + roomOffset < 45) {
                return "Turn around"
            }
            else {
                return "Turn to your right and walk straight"
            }
        }
        
        return ""
    }

    func configuretargetBeaconForValue(beaconValue: String) {
        self.targetBeacon = nil;
        self.playedArrivalAlert = false;
        
        if (self.frontLeftBeacon!.name == beaconValue) {
            self.targetBeacon = self.frontLeftBeacon
        }
        else if (self.backLeftBeacon!.name == beaconValue) {
            self.targetBeacon = self.backLeftBeacon
        }
        else if (self.frontRightBeacon!.name == beaconValue) {
            self.targetBeacon = self.frontRightBeacon
        }
        else if (self.backRightBeacon!.name == beaconValue) {
            self.targetBeacon = self.backRightBeacon
        }
    }
}

