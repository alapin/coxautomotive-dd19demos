//
//  ObjCBridge.h
//  digitaldealerdemo
//
//  Created by Lapin, Andy (KBB) on 9/28/15.
//  Copyright © 2015 Kelley Blue Book. All rights reserved.
//

#ifndef ObjCBridge_h
#define ObjCBridge_h

//#import <EstimoteSDK/EstimoteSDK.h>
#import "ESTBeaconManager.h"
#import "ESTBeaconRegion.h"
#import "ESTBeacon.h"
#import "ESTIndoorLocationManager.h"
#import "ESTLocation.h"
#import "ESTIndoorLocationView.h"
#import "ESTLocationBuilder.h"
#import "ESTConfig.h"

//#import <ApiAI/ApiAI.h>
//#import <ApiAI/AIDefaultConfiguration.h>
//#import <ApiAI/AIVoiceRequestButton.h>

#endif /* ObjCBridge_h */
