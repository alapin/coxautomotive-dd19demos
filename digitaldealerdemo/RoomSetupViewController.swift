//
//  RoomSetupViewController.swift
//  digitaldealerdemo
//
//  Created by Lapin, Andy (KBB) on 10/2/15.
//  Copyright © 2015 Kelley Blue Book. All rights reserved.
//

import UIKit
import UIView_DragDrop
import EVReflection

class RoomSetupViewController: UIViewController, UIViewDragDropDelegate {

    @IBOutlet var roomOutlineImage: UIImageView!
    
    @IBOutlet var blueAContainer: UIView!
    @IBOutlet var greenBContainer: UIView!
    @IBOutlet var greenAContainer: UIView!
    @IBOutlet var purpleAContainer: UIView!
    
    @IBOutlet var greenAVertical: NSLayoutConstraint!
    @IBOutlet var greenAHorizontal: NSLayoutConstraint!
    @IBOutlet var greenBHorizontal: NSLayoutConstraint!
    @IBOutlet var greenBVertical: NSLayoutConstraint!
    @IBOutlet var purpleAVertical: NSLayoutConstraint!
    @IBOutlet var purpleAHorizontal: NSLayoutConstraint!
    @IBOutlet var blueAHorizontal: NSLayoutConstraint!
    @IBOutlet var blueAVertical: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.roomOutlineImage.image = self.drawCustomImage(self.roomOutlineImage.frame.size)
        
        self.blueAContainer.makeDraggable()
        self.blueAContainer.setDelegate(self)
        self.blueAContainer.setDropViews([self.roomOutlineImage])
        self.greenAContainer.makeDraggable()
        self.greenAContainer.setDelegate(self)
        self.greenAContainer.setDropViews([self.roomOutlineImage])
        self.greenBContainer.makeDraggable()
        self.greenBContainer.setDelegate(self)
        self.greenBContainer.setDropViews([self.roomOutlineImage])
        self.purpleAContainer.makeDraggable()
        self.purpleAContainer.setDelegate(self)
        self.purpleAContainer.setDropViews([self.roomOutlineImage])
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let config: BeaconConfiguration = (UIApplication.sharedApplication().delegate as! AppDelegate).beaconConfiguraiton!
        if (config.beacons?.count > 0) {
            for item in config.beacons!
            {
                let beacon: Beacon = item as Beacon
                if (beacon.name == "Jetta") {
                    self.blueAHorizontal.constant = CGFloat(beacon.x)
                    self.blueAVertical.constant = CGFloat(beacon.y)
                    NSLog("%.0f, %.0f", self.blueAContainer.center.x, self.blueAContainer.center.y)
                    
                }
                else if ((item as Beacon).name == "Camry") {
                    self.greenAHorizontal.constant = CGFloat(beacon.x)
                    self.greenAVertical.constant = CGFloat(beacon.y)

                }
                else if ((item as Beacon).name == "Volt") {
                    self.greenBHorizontal.constant = CGFloat(beacon.x)
                    self.greenBVertical.constant = CGFloat(beacon.y)

                }
                else if ((item as Beacon).name == "Sales") {
                    self.purpleAHorizontal.constant = CGFloat(beacon.x)
                    self.purpleAVertical.constant = CGFloat(beacon.y)
                }
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        let config: BeaconConfiguration = BeaconConfiguration()
        config.beacons?.append(Beacon(name: "Jetta", x: self.blueAContainer.frame.origin.x, y: self.blueAContainer.frame.origin.y, major: self.beaconForValue("Jetta")!))
        config.beacons?.append(Beacon(name: "Camry", x: self.greenAContainer.frame.origin.x, y: self.greenAContainer.frame.origin.y, major: self.beaconForValue("Camry")!))
        config.beacons?.append(Beacon(name: "Volt", x: self.greenBContainer.frame.origin.x, y: self.greenBContainer.frame.origin.y, major: self.beaconForValue("Volt")!))
        config.beacons?.append(Beacon(name: "Sales", x: self.purpleAContainer.frame.origin.x, y: self.purpleAContainer.frame.origin.y, major: self.beaconForValue("Sales")!))
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).beaconConfiguraiton = config
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func view(view: UIView, wasDroppedOnDropView dropView: UIView) {
        NSLog("View %@ was dropped at %.0f,%.0f", view.description, view.center.x, view.center.y)
        
    }
    
    func draggingDidEndWithoutDropForView(view: UIView) {
        NSLog("View %@ was dropped at %ld,%ld", view.description, view.frame.origin.x, view.frame.origin.y)
    }
    
    func drawCustomImage(size: CGSize) -> UIImage {
        // Setup our context
        let lineWidth: CGFloat = 2.0
        let bounds = CGRect(origin: CGPointMake(CGPoint.zero.x + lineWidth / 2.0, CGPoint.zero.y + lineWidth / 2.0), size: CGSizeMake(size.width - lineWidth, size.height - lineWidth))
        let opaque = false
        let scale: CGFloat = 0
        UIGraphicsBeginImageContextWithOptions(size, opaque, scale)
        let context = UIGraphicsGetCurrentContext()
        
        // Setup complete, do drawing here
        CGContextSetStrokeColorWithColor(context, UIColor.blackColor().CGColor)
        CGContextSetLineWidth(context, 2.0)
        
        CGContextStrokeRect(context, bounds)
        
        // Drawing complete, retrieve the finished image and cleanup
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        return UIInterfaceOrientationMask.Portrait;
    }
    
    override func shouldAutorotate() -> Bool {
        return false;
    }
    
    func beaconForValue(beaconValue: NSString) -> NSNumber? {
        if (beaconValue.isEqualToString("Jetta")) {
            return NSNumber(integer: 47301)
        }
        else if (beaconValue.isEqualToString("Camry")) {
            return NSNumber(integer: 10331)
        }
        if (beaconValue.isEqualToString("Volt")) {
            return NSNumber(integer: 61998)
        }
        if (beaconValue.isEqualToString("Sales")) {
            return NSNumber(integer: 1886)
        }
        
        return nil
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
