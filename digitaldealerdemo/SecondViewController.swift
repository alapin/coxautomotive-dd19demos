//
//  SecondViewController.swift
//  digitaldealerdemo
//
//  Created by Lapin, Andy (KBB) on 9/25/15.
//  Copyright © 2015 Kelley Blue Book. All rights reserved.
//

import UIKit
import ApiAI
import CoreLocation
import AVFoundation
import ApiAI

class SecondViewController: UIViewController, ESTBeaconManagerDelegate, CLLocationManagerDelegate  {

    let beaconManager: ESTBeaconManager = ESTBeaconManager()
    let locationManager: CLLocationManager = CLLocationManager()
    @IBOutlet var requestVoiceButton: AIVoiceRequestButton!
    @IBOutlet var infoTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.infoTextView?.text = ""

        // Start ranging beacons
        let region: ESTBeaconRegion = ESTBeaconRegion(proximityUUID: ESTIMOTE_PROXIMITY_UUID, identifier: "EstimoteSampleRegion");
        self.beaconManager.delegate = self;
        self.beaconManager.returnAllRangedBeaconsAtOnce = true;
        
        // Start ranging beacons
        if (ESTBeaconManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined) {
            self.beaconManager.requestAlwaysAuthorization();
        }
        else if (ESTBeaconManager.authorizationStatus() == CLAuthorizationStatus.Denied) {
            let alertController = UIAlertController(title: "Location Error", message:
                "Please allow access to location services", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil));
            
            self.presentViewController(alertController, animated: true, completion: nil);
        }
        else {
            self.beaconManager.startRangingBeaconsInRegion(region);
            
            // Start tracking heading
            self.locationManager.startUpdatingHeading()
            self.locationManager.delegate = self
        }
        
        self.requestVoiceButton?.color = UIColor.yellowColor();
        self.requestVoiceButton?.iconColor = UIColor .blueColor();
        
        self.requestVoiceButton?.successCallback = {(AnyObject response) -> Void in
            NSLog(response.description)
            let responseDict: NSDictionary = response as! NSDictionary
            let year: String = ApiResponseHelper.yearFromApiAi(responseDict)
            let make: String = ApiResponseHelper.makeFromApiAi(responseDict)
            let modelName: String = ApiResponseHelper.modelFromApiAi(responseDict)
            let intentName: String = ApiResponseHelper.valueFromApiAiMetadata("intentName", withDictionary: responseDict)
            
            if (intentName == "FindOnLot") {
                self.infoTextView.text = String(format: "You searched for %@ %@ %@ and we have some in stock\n", year, make, modelName) + self.infoTextView.text
                
//                let voice : String = self.directionToBeaconForValue(modelName)
//                print(voice)
//                let utterance: AVSpeechUtterance = AVSpeechUtterance(string: voice)
//                self.synth.speakUtterance(utterance)
//                self.configuretargetBeaconForValue(modelName)
            }
            else if (intentName == "Buy Now") {
                self.infoTextView.text = String(format: "You found the sales intent for %@ %@ %@\n", year, make, modelName) + self.infoTextView.text
                
//                let voice : String = self.directionToBeaconForValue("Sales")
//                let utterance: AVSpeechUtterance = AVSpeechUtterance(string: voice)
//                let synth: AVSpeechSynthesizer = AVSpeechSynthesizer()
//                synth.speakUtterance(utterance)
//                self.configuretargetBeaconForValue("Sales")
            }
            else {
                self.infoTextView.text = "Sorry, I didn't understand what you were asking. Please try again\n" + self.infoTextView.text
            }
        }
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        return UIInterfaceOrientationMask.Portrait;
    }
    
    override func shouldAutorotate() -> Bool {
        return false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func beaconManager(manager: ESTBeaconManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: ESTBeaconRegion!) {
        if (beacons.count > 0) {
            for item in beacons {
                let currentBeacon: ESTBeacon = item as! ESTBeacon
                self.infoTextView!.text = String(format: "major: %.0f minor: %.0f distance: %.0f\n", currentBeacon.major!.floatValue, currentBeacon.major!.floatValue, currentBeacon.distance!.floatValue) + self.infoTextView!.text
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.infoTextView!.text = String(format: "Heading: %.2f \n", newHeading.trueHeading) + self.infoTextView!.text
        
    }
    
}

